import React, { useState } from 'react';
import Person from './Person';
import data from './data.json';
import './styles.css';
import fs from 'fs';

const Trombinoscope = () => {
  const [personnes, setPersonnes] = useState(data);

  const handleUpdatePerson = (id, editedPrenom, editedNom) => {
    const updatedPersonnes = personnes.map(person => {
      if (person.id === id) {
        return {
          ...person,
          prenom: editedPrenom,
          nom: editedNom,
        };
      }
      return person;
    });

    setPersonnes(updatedPersonnes);
    saveDataToJson(updatedPersonnes);
  };

  // Fonction pour sauvegarder les données dans le fichier data.json
  const saveDataToJson = updatedData => {
    try {
      fs.writeFileSync('./src/data.json', JSON.stringify(updatedData, null, 2));
      console.log('Data successfully saved to data.json');
    } catch (error) {
      console.error('Error saving data to data.json', error);
    }
  };

  return (
    <div className="trombinoscope">
      <h1>Trombinoscope</h1>
      {personnes.map(person => (
        <Person key={person.id} onUpdatePerson={handleUpdatePerson} {...person} />
      ))}
    </div>
  );
};

export default Trombinoscope;