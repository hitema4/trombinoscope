import React from 'react';
import Trombinoscope from './Trombinoscope';

function App() {
  return (
    <div className="App">
      <Trombinoscope />
    </div>
  );
}

export default App;
