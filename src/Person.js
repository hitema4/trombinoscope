// Person.js
import React, { useState } from 'react';
import './styles.css';

const Person = ({ id, prenom, nom, photo, onUpdatePerson }) => {
  const [editedPrenom, setEditedPrenom] = useState(prenom);
  const [editedNom, setEditedNom] = useState(nom);
  const [isEditing, setIsEditing] = useState(false);

  const handleEdit = () => {
    setIsEditing(true);
  };

  const handleSave = () => {
    onUpdatePerson(id, editedPrenom, editedNom);
    setIsEditing(false);
  };

  return (
    <div className="person">
      <img src={photo} alt={`${editedPrenom} ${editedNom}`} />
      {isEditing ? (
        <div className="form-container">
          <input
            type="text"
            value={editedPrenom}
            onChange={(e) => setEditedPrenom(e.target.value)}
            className="form-input"
          />
          <input
            type="text"
            value={editedNom}
            onChange={(e) => setEditedNom(e.target.value)}
            className="form-input"
          />
          <button onClick={handleSave} className="form-button">
            Enregistrer
          </button>
        </div>
      ) : (
        <>
          <p>{editedPrenom} {editedNom}</p>
          <button onClick={handleEdit}>Modifier</button>
        </>
      )}
    </div>
  );
};

export default Person;
